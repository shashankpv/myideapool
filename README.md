# MyIdeaPool
### by [Shashank PV](shashank.pv@gmail.com)
Solution to the [Sample Backend Project](https://www.codementor.io/codementorx/draft/ixvyx7tvj?utm_swu=4667)

# Build
> docker build --rm -f "Dockerfile" -t myideapool:latest .

# Run
> docker run -p 5000:5000  myideapool:latest

# Verify
- Import the included Postman collection
- Use the [cm-quiz](https://github.com/codementordev/cm-quiz) testing tool