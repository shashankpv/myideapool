from models import UserModel, RevokedTokenModel, IdeaModel
from utils import Parser
from libgravatar import Gravatar
from flask_restful import Resource
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)


class Signup(Resource):

    def post(self):
        parser = Parser.user_parser()
        parser.add_argument('name', help = 'This field cannot be blank', required = True)
        data = parser.parse_args()
        
        if UserModel.find_by_email(data['email']):
            return {'message': 'User {0} already exists'.format(data['email'])}, 400
        
        if not UserModel.is_valid_email(data['email']):
            return {'message': 'Invalid email'}, 400      

        password_validation_message = UserModel.validate_password(data['password'])
        if password_validation_message:
            return {'message': password_validation_message}, 400      

        new_user = UserModel(
            email = data['email'],
            name = data['name'],
            password = UserModel.generate_hash(data['password'])            
        )
        
        try:
            new_user.save_to_db()
            access_token = create_access_token(identity = data['email'])
            refresh_token = create_refresh_token(identity = data['email'])
            return {
                'jwt': access_token,
                'refresh_token': refresh_token
                }, 201
        except:
            return {'message': 'Something went wrong'}, 500


class Login(Resource):

    def post(self):
        parser = Parser.user_parser()
        data = parser.parse_args()

        current_user = UserModel.find_by_email(data['email'])

        if not current_user:
            return {'message': 'User {0} doesn\'t exist'.format(data['email'])}, 400
        
        if UserModel.verify_hash(data['password'], current_user.password):
            access_token = create_access_token(identity = data['email'])
            refresh_token = create_refresh_token(identity = data['email'])
            return {
                'jwt': access_token,
                'refresh_token': refresh_token
                }, 201
        else:
            return {'message': 'Incorrect credentials'}, 400

    @jwt_required
    def delete(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti = jti)
            revoked_token.add()
            return {'message': 'Refresh token has been revoked'}, 204
        except:
            return {'message': 'Something went wrong'}, 500


class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user_email = get_jwt_identity()
        access_token = create_access_token(identity = current_user_email)
        return {'jwt': access_token}


class CurrentUser(Resource):
    @jwt_required
    def get(self):
        current_user_email = get_jwt_identity()
        current_user_name = UserModel.find_by_email(current_user_email).name
        gravatar_url = Gravatar(current_user_email).get_image()

        return {
            'email': current_user_email,
            'name': current_user_name,
            'avatar_url': gravatar_url
        }, 200


class IdeaList(Resource):

    @jwt_required
    def get(self, page=1):
        user = UserModel.find_by_email(get_jwt_identity())
        ideas = IdeaModel.return_page(user_id=user.id, page_num=page)
        return ideas, 200

    @jwt_required
    def post(self):
        
        parser = Parser.create_idea_parser()
        data = parser.parse_args()

        try:
            user = UserModel.find_by_email(get_jwt_identity())
            new_idea = IdeaModel(
                content = data['content'],
                impact = data['impact'],
                ease = data['ease'],
                confidence = data['confidence'],
                user_id = user.id
            )
            result = new_idea.save_to_db()
            return result, 201
        except Exception as err:
            return {'message': 'Something went wrong {0}'.format(err)}, 500


class Idea(Resource):

    @jwt_required
    def put(self, idea_id):
        parser = Parser.create_idea_parser()
        data = parser.parse_args()

        try:
            user = UserModel.find_by_email(get_jwt_identity())

            message = IdeaModel.check_access(idea_id, user.id)
            if message:
                return {'message': message}, 400

            idea = IdeaModel(
                id = idea_id,
                content = data['content'],
                impact = data['impact'],
                ease = data['ease'],
                confidence = data['confidence'],
                user_id = user.id
            )

            result = IdeaModel.update(idea)
            return result, 200
        except Exception as err:
            return {'message': 'Something went wrong {0}'.format(err)}, 500


    @jwt_required
    def delete(self, idea_id):        
        try:
            user = UserModel.find_by_email(get_jwt_identity())
            
            message = IdeaModel.check_access(idea_id, user.id)
            if message:
                return {'message': message}, 400

            IdeaModel.delete_by_id(idea_id, user.id)
            return '', 204
        except:
            return {'message': 'Something went wrong'}, 500