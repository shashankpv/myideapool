from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'top-secret-key'
app.config['JWT_SECRET_KEY'] = 'top-secret-jwt-key'
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
app.config['JWT_TOKEN_LOCATION'] = ('headers', 'json')
app.config['JWT_HEADER_NAME'] = 'X-Access-Token'
app.config['JWT_HEADER_TYPE'] = ''
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = 600 # 10 minutes

api = Api(app)
db = SQLAlchemy(app)
jwt = JWTManager(app)

#TODO:
@jwt.expired_token_loader
def handle_expired_error():
    return {'message': 'Token has expired'}, 401
        
@app.before_first_request
def create_tables():
    db.create_all()

@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return models.RevokedTokenModel.is_jti_blacklisted(jti)


import views, models, resources

api.add_resource(resources.Signup, '/users')
api.add_resource(resources.Login, '/access-tokens')
api.add_resource(resources.TokenRefresh, '/access-tokens/refresh')
api.add_resource(resources.CurrentUser, '/me')
api.add_resource(resources.Idea, '/ideas/<string:idea_id>')
api.add_resource(resources.IdeaList, '/ideas', '/ideas/<int:page>')