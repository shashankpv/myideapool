import time
import datetime
import re
import uuid
from app import db
from passlib.hash import pbkdf2_sha256 as sha256
from validate_email import validate_email

class UserModel(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key = True)
    email = db.Column(db.String(255), unique = True, nullable = False)
    name = db.Column(db.String(255), nullable = False)
    password = db.Column(db.String(255), nullable = False)
    
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by_email(cls, email):
        return cls.query.filter_by(email = email).first()
    
    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'email': x.email,
                'name': x.name,
                'password': x.password
            }
        return {'users': list(map(lambda x: to_json(x), UserModel.query.all()))}

    @classmethod
    def delete_all(cls):
        num_rows_deleted = db.session.query(cls).delete()
        db.session.commit()
        return {'message': '{} row(s) deleted'.format(num_rows_deleted)}

    @staticmethod
    def generate_hash(password):
        return sha256.hash(password)
    
    @staticmethod
    def verify_hash(password, hash):
        return sha256.verify(password, hash)

    @staticmethod
    def validate_password(password):

        if not password:
            return 'Password not provided'

        if not re.match('.*\d.*', password):
            return 'Password must contain 1 number'

        if not re.match('.*[A-Z].*', password):
            return 'Password must contain 1 capital letter'

        if len(password) < 8 or len(password) > 50:
            return 'Password must be between 8 and 50 characters'

        return None

    @staticmethod
    def is_valid_email(email):        
        return validate_email(email)

class RevokedTokenModel(db.Model):
    __tablename__ = 'revoked_tokens'
    id = db.Column(db.Integer, primary_key = True)
    jti = db.Column(db.String(120))
    
    def add(self):
        db.session.add(self)
        db.session.commit()
    
    @classmethod
    def is_jti_blacklisted(cls, jti):
        query = cls.query.filter_by(jti = jti).first()
        return bool(query)


class IdeaModel(db.Model):
    __tablename__ = 'ideas'

    id = db.Column(db.String(32), default=lambda: uuid.uuid4().hex, primary_key = True)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)
    content = db.Column(db.String(255), nullable = False)
    impact = db.Column(db.Integer, nullable = False)
    ease = db.Column(db.Integer, nullable = False)
    confidence = db.Column(db.Integer, nullable = False)
    average_score = db.Column(db.Float, nullable = False)
    created_at = db.Column(db.DateTime, default=lambda: datetime.datetime.now(), nullable = False)

    def save_to_db(self):
        self.average_score = round(float(self.impact + self.ease + self.confidence)/3, 2)
        db.session.add(self)
        db.session.commit()
        return self.to_json()
    
    def to_json(self):
        return {
            'content': self.content,
            'impact': self.impact,
            'ease': self.ease,
            'confidence': self.confidence,
            'average_score': self.average_score,
            'created_at': time.mktime(self.created_at.timetuple()),
            'id': self.id
        }

    @classmethod
    def return_page(cls, user_id, page_num, page_size=10):
        results = IdeaModel.query \
                           .filter_by(user_id=user_id) \
                           .order_by(IdeaModel.average_score.desc()) \
                           .offset(page_size*(page_num-1)) \
                           .limit(page_size) \
                           .all()
        return list(map(lambda x: x.to_json(), results))

    @classmethod
    def update(cls, idea):
        updated_idea = cls.query.filter_by(id = idea.id).first()
        
        if updated_idea.user_id != idea.user_id:
            raise Exception("Can only update your Ideas")
            
        updated_idea.content = idea.content
        updated_idea.impact = idea.impact
        updated_idea.ease = idea.ease
        updated_idea.confidence = idea.confidence
        updated_idea.average_score = round(float(idea.impact + idea.ease + idea.confidence)/3, 2)
        db.session.commit()
        return updated_idea.to_json()

    @classmethod
    def check_access(cls, idea_id, user_id):
        found_idea = cls.query.filter_by(id = idea_id).first()
        if not found_idea:
            return 'No Idea found'

        if found_idea.user_id != user_id:
            return 'Can only access your Ideas'
        
        return None

    @classmethod
    def delete_by_id(cls, idea_id, user_id):
        delete_idea = cls.query.filter_by(id = idea_id).first()
        
        if delete_idea.user_id != user_id:
            raise Exception("Can only delete your Ideas")

        db.session.delete(delete_idea)
        db.session.commit()