from flask_restful import reqparse

class Parser():

    @staticmethod
    def user_parser():
        parser = reqparse.RequestParser()
        parser.add_argument('email', help = 'This field cannot be blank', required = True)
        parser.add_argument('password', help = 'This field cannot be blank', required = True)
        return parser
    
    @staticmethod
    def create_idea_parser():
        parser = reqparse.RequestParser()
        parser.add_argument('content', help = 'This field cannot be blank', required = True)
        parser.add_argument('impact', type=Parser.score_in_range, help = 'This field should be in range [1, 10]', required = True)
        parser.add_argument('ease', type=Parser.score_in_range, help = 'This field should be in range [1, 10]', required = True)
        parser.add_argument('confidence', type=Parser.score_in_range, help = 'This field should be in range [1, 10]', required = True)
        return parser
    
    @staticmethod
    def score_in_range(score):
        score = int(score)
        if score < 1:
            raise ValueError("Minimum score value is 1")

        if score > 10:
            raise ValueError("Maximum score value is 10")

        return score