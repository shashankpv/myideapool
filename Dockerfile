FROM python:3

ADD . /app
WORKDIR /app

RUN pip install --no-cache-dir -r requirements.txt

ENV FLASK_ENV=development
EXPOSE 5000
CMD cd server && flask run --host=0.0.0.0